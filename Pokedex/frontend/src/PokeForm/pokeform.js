import React, {Component} from 'react'
import pokeFormStyle from './pokeFormStyle.css'
class PokeForm extends Component {

    render(){
        return(
            <div className="pokeForm">
                <form className="pokeForm--inputs">
                    <input placeholder="Name"></input>
                    <input placeholder="Height"></input>
                    <input placeholder="Weight"></input>
                    <input placeholder="Abillities"></input>
                    <input placeholder="Habitat"></input>
                    <input placeholder="Image"></input>
                    <button>Add to pokedex</button>
                </form>
            </div>
        )
    }
}

export default PokeForm