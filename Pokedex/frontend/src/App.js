import React, {Component} from 'react'
import PokeForm from './PokeForm/pokeform'
import PokeList from './PokeList/pokelist'



class App extends Component{
  render() {
    return(
      <div className="main">
          <h1 className="header--title">Pokemon API</h1>
          <PokeForm />
          <PokeList />
      </div>
      
    )
  }
}

export default App;
