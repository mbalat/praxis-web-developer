import React, {Component} from 'react'
import pokeListStyle from './pokeListStyle.css'

class PokeList extends Component{
    render(){
        return(
            <div >
                <ul className="pokeList">
                    <li>Pokemon 1</li>
                    <li>Pokemon 2</li>
                    <li>Pokemon 3</li>
                </ul>
            </div>
        )
    }
}

export default PokeList