const express = require('express')
const port = process.env.PORT || 5000
const mainRoute = '/api/pokemon'
const mongoose = require('mongoose')

const pokemonController = require('./controllers/pokemon') 

mongoose.connect('mongodb://localhost/pokedex',
{ useNewUrlParser: true, useUnifiedTopology: true })
.then(() => {
    const app = express()
    app.use(express.json())

    app.get(mainRoute, pokemonController.findPokemons)
    app.get(`${mainRoute}/:name`, pokemonController.findPokemon)
    app.post(mainRoute, pokemonController.createPokemon)
    app.put(`${mainRoute}/:name`, pokemonController.updatePokemon)
    app.delete(`${mainRoute}/:name`, pokemonController.deletePokemon)

    app.listen(port, () => {
        console.log(`http://localhost:${port}`)
    })
})
.catch(() => {
    console.log('DB connection failed')
})

