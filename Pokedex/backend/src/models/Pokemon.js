const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    name: String,
    height: Number,
    weight: Number,
    habitat: String,
    abilities: [],
    image: String
})

module.exports = mongoose.model('pokemons', schema)