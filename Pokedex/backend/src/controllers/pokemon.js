const Pokemon = require('../models/Pokemon')

// FIND AND LIST ALL POKEMONS FROM DB
exports.findPokemons = async (req, res) => {
    const pokemon = await Pokemon.find()
    res.send({pokemons: pokemon})
}

// CREATE NEW POKEMON
exports.createPokemon = async (req, res) => {
    const pokemon = new Pokemon(req.body)
    await pokemon.save()
    res.send(pokemon)
}

//  GET SINGLE POKEMON BY NAME
exports.findPokemon = async (req, res) => {
    try {
        const pokemon = await Pokemon.findOne({name: req.params.name})
        res.send(pokemon)
    } catch {
        //NE RADI ???
        res.status(404).send({error: "Such pokemon does not exist"})
    }
}

// UPDATE POKEMON
exports.updatePokemon = async (req, res) => {
   try {
        const pokemon = await Pokemon.findOneAndUpdate({name: req.params.name}, req.body)
        res.send(pokemon)
   } catch {
        //NE RADI ???
        res.status(404).send({error: "Update failed"})
    }
}

// DELETE POKEMON
exports.deletePokemon = async (req, res) => {
    try {
        const pokemon = await Pokemon.findOne({name: req.params.name})
        await pokemon.remove()
        res.send(true)
       
    } catch {
        res.status(404).send({error: "Delete failed"})
    }
}