import React from 'react'
const validation = (props) => {
    let validiationMessage = 'Text long enough'
    if( props.inputLength <= 5){
        validiationMessage = 'Text too short'
    }
    return (
        <div>
            <p>{validiationMessage}</p>
        </div>
    );
}

export default validation;