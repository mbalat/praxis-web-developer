//Book constructor
function Book(title,author,isbn){
    this.author = author;
    this.title = title;
    this.isbn = isbn;
}

//UI constructor
function UI(){
    //Add book to list
    UI.prototype.addBookToList = function(book){
        
        const list = document.querySelector('#book-list');
        //Create tr element
        const row = document.createElement('tr');
        //Insert cols
        row.innerHTML = `
        <td>${book.title}</td>
        <td>${book.author}</td>
        <td>${book.isbn}</td>
        <td><a href="#" class="delete">X</td>

        `;
        list.appendChild(row);
    }

    //Delete book
    UI.prototype.deleteBook = function(target){
        if(target.className === 'delete'){
            target.parentElement.parentElement.remove();
        }
    }

    //Clear Fields
    UI.prototype.dleteteFields = function(){
        document.querySelector('#title').value = '';
        document.querySelector('#author').value = '';
        document.querySelector('#isbn').value = '';

    }

    // Show alert
    UI.prototype.showAlert = function(message, className){
        //Create div
        const div = document.createElement('div');
        //Add classes
        div.className = `alert ${className}`;
        //Add text
        div.appendChild(document.createTextNode(message));
        // Get parent
        const container = document.querySelector('.container');
        const form = document.querySelector('#book-form');
        //Insert alert
        container.insertBefore(div, form);
        //timeout after 3 seconds
        setTimeout(function(){
            document.querySelector('.alert').remove();
        },3000);
    }

}

//Event Listener for add book
document.querySelector('#book-form').addEventListener('submit',
function(e){
    //Get input values
    const title = document.querySelector('#title').value;
    const author = document.querySelector('#author').value;
    const isbn = document.querySelector('#isbn').value;

    //Instatitate book constructor
    const book = new Book(title,author,isbn);

    //Instatiate UI constructor
    const ui = new UI();

    //Validate 
    if(title === '' || author === '' || isbn === ''){
        // Error alert
        ui.showAlert('Please fill in all fields', 'error');
    } else {
        //Add book to list
         ui.addBookToList(book);

         //Show success
         ui.showAlert('Book Added!', 'success');

        //Clear fields
        ui.dleteteFields();
    }
        e.preventDefault();
});

    //Event Listener for Delete
    document.querySelector('#book-list').addEventListener('click', function(e){
        const ui = new UI();

        if (e.target.className === 'delete') {
            ui.deleteBook(e.target);
         
            ui.showAlert('book removed', 'success');
          }
         
          e.preventDefault();
    });

    

