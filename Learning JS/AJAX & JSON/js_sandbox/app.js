document.querySelector('#button1').addEventListener('click', loadCostumer);
document.querySelector('#button2').addEventListener('click', loadCostumers);


function loadCostumer(e){
  const xhr = new XMLHttpRequest();
  xhr.open('GET', 'costumer.json', true);
  xhr.onload = function() {
    if(this.status === 200){
      
      const costumer = JSON.parse(this.responseText);
      const output = `
      
      <ul>
      <li>ID: ${costumer.id}</li>
      <li>Name: ${costumer.name}</li>
      <li>Company: ${costumer.company}</li>
      <li>Phone: ${costumer.phone}</li>
      </ul>
      
      `
      document.querySelector('#costumer').innerHTML = output;
      
    }
  }
  xhr.send();
}


function loadCostumers() {
  const xhr = new XMLHttpRequest();
  xhr.open('GET', 'costumers.json', true);
  xhr.onload = function() {
    if(this.status === 200){
      const costumers = JSON.parse(this.responseText);
      let output = '';
      costumers.forEach(costumer => {
        output +=
        `
      <ul>
      <li>ID: ${costumer.id}</li>
      <li>Name: ${costumer.name}</li>
      <li>Company: ${costumer.company}</li>
      <li>Phone: ${costumer.phone}</li>
      </ul>
    
      `;
      });

      document.querySelector('#costumers').innerHTML = output;
      
    }
  }
  xhr.send();  
}


