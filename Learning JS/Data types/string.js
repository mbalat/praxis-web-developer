// Uppercase the first character
// Write a function ucFirst(str) that returns the string str with the uppercased first character, for instance:



// function ucFirst(str) {
//   return str = str[0].toUpperCase() + str.slice(1);
// };

// console.log(ucFirst('john'));





// Check for spam
// Write a function checkSpam(str) that returns true if str contains ‘viagra’ or ‘XXX’, otherwise false.
// The function must be case-insensitive:

// function checkSpam(str){
//     let lower = str.toLowerCase();

//     if(lower.includes('viagra') || lower.includes('xxx')){
//         return true;
//     } else {
//         return false;
//     }
// };


// checkSpam('buy ViAgRA now') == true
// checkSpam('free xxxxx') == true
// checkSpam("innocent rabbit") == false




// Truncate the text
// Create a function truncate(str, maxlength) that checks the length of the str and, if it exceeds maxlength – replaces the end of str with the ellipsis character "…", to make its length equal to maxlength.
// The result of the function should be the truncated (if needed) string.
// For instance:



// function truncate(str, maxlength){
//     if(str.length > maxlength){
//         return str.slice(0, maxlength-1) + '...';
//     } else {
//         return str;
//     }
// };


// console.log(truncate("What I'd like to tell on this topic is:", 20));
// console.log(truncate("Hi everyone!", 4));








// Extract the money
// We have a cost in the form "$120". That is: the dollar sign goes first, and then the number.
// Create a function extractCurrencyValue(str) that would extract the numeric value from such string and return it.

const price = '$120';

function extractCurrencyValue(str){
    if(str[0] == '$'){
       return str.slice(1);
    } else {
        return str;
    }
}

console.log(extractCurrencyValue(price));

