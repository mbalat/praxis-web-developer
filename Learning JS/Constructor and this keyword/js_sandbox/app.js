//Person constructor

function Person(name,dob){
  this.name = name;
  // this.age = age;
  this.birthday = new Date(dob);
  this.calculateAge = function(){
    const difference = Date.now() - this.birthday.getTime();
    const ageDate = new Date(difference);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }
  
  
}

const matej = new Person('Matej', '03-16-1996');
//console.log(matej);


const name1 = 'Jeff';
const name2 = new String('Jeff');

// name2.foo = 'bar'
// console.log(name2);
// console.log(typeof name2);

// if(name2 === 'Jeff'){
//   console.log('Yes');
// } else {
//   console.log('No');
// }

const num1 = 5;
const num2 = new Number(5);
// console.log(typeof num2);


const bool1 = true;
const bool2 = new Boolean(true);
// console.log( typeof bool2);

const getSum1 = function(x,y){
  return x*y;
}
const getSum2 = new Function('x','y', 'return 1 +1')

// console.log(typeof getSum2(1,1));

const john = {name:"John"};

// console.log(john);


const arr1 = [1,2,3,4];
const arr2 = new Array(1,2,3,4);

//console.log(arr2);

const re1 = /\w+/;
const re2 = new RegExp('\w+');

//console.log(re2);


