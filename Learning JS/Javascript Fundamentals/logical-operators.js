// What's the result of OR?
// What is the code below going to output?
// alert( null || 2 || undefined );

// 2 because its first true value


// What's the result of OR'ed alerts?
// What will the code below output?
//alert( alert(1) || 2 || alert(3) );

// 1 2


// What is the result of AND?
// What is this code going to show?
//alert( 1 && null && 2 );

//null is the first false

// What is the result of AND'ed alerts?
// What will this code show?
// alert( alert(1) && alert(2) );

//1 undefined

// The result of OR AND OR
// What will the result be?
//alert( null || 2 && 3 || 4 );

//3

// Check the range between
// Write an “if” condition to check that age is between 14 and 90 inclusively.
// “Inclusively” means that age can reach the edges 14 or 90.

let age = 20;

if(age >= 14 && age <= 90){
    console.log('You are between 14 and 90 YO.');
    
}

// Check the range outside
// Write an if condition to check that age is NOT between 14 and 90 inclusively.
// Create two variants: the first one using NOT !, the second one – without it.

if(!(age > 14 && age < 90)){
    console.log('You are not between 14 and 90 YO.');
}

if(age < 14 || age > 90 ){
    console.log('You are not between 14 and 90 YO.');
}

// Which of these alerts are going to execute?
// What will the results of the expressions be inside if(...)?

// if (-1 || 0) alert( 'first' ); //first
// if (-1 && 0) alert( 'second' );
// if (null || -1 && 1) alert( 'third' ); //third


// Check the login
// Write the code which asks for a login with prompt.

// If the visitor enters "Admin", then prompt for a password, if the input is an empty line or Esc – show “Canceled”, if it’s another string – then show “I don’t know you”.

// The password is checked as follows:

// If it equals “TheMaster”, then show “Welcome!”,
// Another string – show “Wrong password”,
// For an empty string or cancelled input, show “Canceled”
// The schema:


// Please use nested if blocks. Mind the overall readability of the code.

// Hint: passing an empty input to a prompt returns an empty string ''. Pressing ESC during a prompt returns null.

let login = prompt('Who is there ?', 'Admin');
if(login === 'Admin' ){
    let passowrd = prompt('Please enter the password', 'TheMaster');
    if(passowrd === 'TheMaster'){
        alert('Welcome');
    }else if (passowrd == '' || passowrd == null){
        alert('Canceled')
    }else{
        alert('Wrong password');
    }
}else if(login == '' || login == null){
    alert('Canceled');
}else{
    alert('I don’t know you');
}