// The postfix and prefix forms

// What are the final values of all variables a, b, c and d after the code below?

 let a = 1, b = 1;

let c = ++a; // //a=2
alert(c); //c=2


let d = b++; // b=1
alert(d); //d=1
alert(b); //b=2


// What are the values of a and x after the code below?

 let a = 2;

 let x = 1 + (a *= 2);
console.log(a); // a = 4

console.log(x); //x = 5


// Type conversions

// What are results of these expressions?

"" + 1 + 0 // "10"
"" - 1 + 0 // -1
true + false // 1 + 0 = 1
6 / "3" // 2 
"2" * "3" // 6
4 + 5 + "px" // "9px"
"$" + 4 + 5 // "$45"
"4" - 2 // 2
"4px" - 2 // NaN
7 / 0 // Infinity
"  -9  " + 5 // "  -9  5"
"  -9  " - 5 // -14
null + 1 // 1
undefined + 1 // NaN
" \t \n" - 2 // -2
// Think well, write down and then compare with the answer.
