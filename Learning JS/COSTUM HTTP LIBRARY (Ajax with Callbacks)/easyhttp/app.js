const http = new easyHTTP;

//Get Posts
// http.get('https://jsonplaceholder.typicode.com/posts',
// function(err, posts){
//     if(err){
//        console.log(err);
//     } else {
//        console.log(posts);
//     }
// });

//GET single post
// http.get('https://jsonplaceholder.typicode.com/posts/1',
// function(err, posts){
//     if(err){
//        console.log(err);
//     } else {
//        console.log(posts);
//     }
// });

//Create Data
const data = {
    title: 'Costum post',
    body: 'This is a costum post'
};

//Create post
// http.post('https://jsonplaceholder.typicode.com/posts', 
// data, function(err, post){
//     if(err) {
//        console.log(err);
//     } else {
//        console.log(post);
//     }
// });

// Put (update) Post
// http.put('https://jsonplaceholder.typicode.com/posts/10', 
// data, function(err, post){
//     if(err) {
//         console.log(err);
//     } else {
//         console.log(post);
//     }
// });

//Delete post
http.delete('https://jsonplaceholder.typicode.com/posts/1',
function(err, response){
    if(err){
       console.log(err);
    } else {
       console.log(response);
    }
});