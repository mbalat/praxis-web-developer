class Person {
    constructor(firstName, lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }
    greeting(){
        return `Hello there ${this.firstName} ${this.lastName}`;
    }
}

class Costumer extends Person {
    constructor(firstName, lastName, phone, membership){
        super(firstName, lastName);

        this.phone = phone;
        this.membership = membership;
    }
    
    static getMembershipCost(){
        return 5000;
    }

}

const john = new Costumer('John', 'Doe', '555-555-555', 'Standard');
console.log(john.greeting());
console.log(Costumer.getMembershipCost());
