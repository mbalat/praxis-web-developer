// // Person constructor

// function Person(firstName, lastName){
//   this.firstName = firstName;
//   this.lastName = lastName;

// }


// Person.prototype.greeting = function(){
//   return `Hello there ${this.firstName} ${this.lastName}`;
// };


// const person1 = new Person('John', 'Doe');
// console.log(person1.greeting());

// //Costumer Constructor

// function Costumer(firstName, lastName, phone, membership){
//   Person.call(this, firstName, lastName);
//   this.phone = phone;
//   this.membership = membership;
// }

// Costumer.prototype = Object.create(Person.prototype);

// Costumer.prototype.constructor = Costumer;

// const costumer1 = new Costumer('Tom', 'Smith', '555-555-555', 'standard');

// function CostumersWife(firstName, lastName){
//   Person.call(this, firstName, lastName);

// }



// const wife1 = new CostumersWife('Jana', 'smith');
// CostumersWife.prototype.greeting = function(){
//   return `Hello there ${costumer1.firstName} ${costumer1.lastName}, I'm good`;
// }




// Costumer.prototype.greeting = function() {
//   return `Hello there ${wife1.firstName} ${wife1.lastName} how are you ?`;
// }

// console.log(costumer1.greeting());
// console.log(wife1.greeting());

const personPrototypes = {
  greeting: function(){
    return `Hello there ${this.firtstName} ${this.lastName}`;
  },
  getsMarried: function(newLastName){
    this.lastName = newLastName;
  }
}

const mary = Object.create(personPrototypes);
mary.firtstName = 'Mary';
mary.lastName = 'Williams';
mary.age = 30;

mary.getsMarried('Thompson');


console.log(mary.greeting());

const brad = Object.create(personPrototypes, {
  firtstName: {value: 'Brad'},
  lastName: {value: 'Traversy'},
  age: {value: 36}
});

console.log(brad);

console.log(brad.greeting());
