// Hello, object
// Write the code, one line for each action:

// Create an empty object user.
// Add the property name with the value John.
// Add the property surname with the value Smith.
// Change the value of the name to Pete.
// Remove the property name from the object.


// let user = {
//     name : 'John',
//     surname: 'Smith'
// }

// user.name = 'Pete';
// delete user.name;
// console.log(user);







// Check for emptiness
// Write the function isEmpty(obj) which returns true if the object has no properties, false otherwise.

// Should work like that:

// let schedule = {};

// alert( isEmpty(schedule) ); // true

// schedule["8:30"] = "get up";

// alert( isEmpty(schedule) ); // false

// let schedule = {};

// // function isEmpty(obj){
// //     if(obj = {} ){

// //         return true;
    
// //     } else {
// //         return false;
// //     }
// // };

// function isEmpty(obj){
//     for( let key in obj){
//         return false;
//     }
//     return true;
// }

// alert(isEmpty(schedule));







// Constant objects?
// Is it possible to change an object declared with const? What do you think?


// const user = {
//   name: "John"
// };

// // does it work?
// user.name = "Pete";

// // Ovaj primjer radi jer const štiti samo variablu , a ne njezin sadržaj koji je u ovom slučaju objekt.
// // S objektom koji je referenciran pomoću const varijable možemo manipulirati
// //Referencu na objekt ne možemo mjenjati jer ćemo kao rezultat dobiti error

// //Primjer s kojim ćemo dobiti error
//  user = admin;








// // Sum object properties
// // We have an object storing salaries of our team:

// let salaries = {
//   John: 100,
//   Ann: 160,
//   Pete: 130
// }
// // Write the code to sum all salaries and store in the variable sum. Should be 390 in the example above.
// // If salaries is empty, then the result must be 0.

// let sum = 0;

// for(let key in salaries){
//      sum += salaries[key]; 
// }
// alert(sum); 










// // Multiply numeric properties by 2
// // Create a function multiplyNumeric(obj) that multiplies all numeric properties of obj by 2.

// // For instance:

// // before the call
// let menu = {
//   width: 200,
//   height: 300,
//   title: "My menu"
// };

// // multiplyNumeric(menu);

// // // after the call
// // menu = {
// //   width: 400,
// //   height: 600,
// //   title: "My menu"
// // };
// // Please note that multiplyNumeric does not need to return anything. It should modify the object in-place.

// // P.S. Use typeof to check for a number here.

// function multiplyNumeric(obj){
//     for(let key in obj){
//         if(typeof obj[key] == 'number'){

//             obj[key] *= 2;
           
//         }
//     }
//     console.log(obj);
// }

// multiplyNumeric(menu);