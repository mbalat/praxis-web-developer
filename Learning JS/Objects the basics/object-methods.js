// Syntax check
// importance: 2
// What is the result of this code?

// let user = {
//   name: "John",
//   go: function() { alert(this.name) }
// }

// (user.go)()





//error




// Explain the value of "this"
// In the code below we intend to call obj.go() method 4 times in a row.
// But calls (1) and (2) works differently from (3) and (4). Why?

// let obj, method;

// obj = {
//   go: function() { alert(this); }
// };

// obj.go();               // (1) [object Object]

// (obj.go)();             // (2) [object Object]

// (method = obj.go)();    // (3) undefined

// (obj.go || obj.stop)(); // (4) undefined





// Zato što su 1 i 2 regularni pozivi metode







// Using "this" in object literal
// Here the function makeUser returns an object.
// What is the result of accessing its ref? Why?

// function makeUser() {
//   return {
//     name: "John",
//     ref: this
//   };
// };

// let user = makeUser();

// alert( user.ref.name ); // What's the result?




//Error 







// Create a calculator
// Create an object calculator with three methods:

// read() prompts for two values and saves them as object properties.
// sum() returns the sum of saved values.
// mul() multiplies saved values and returns the result.
// let calculator = {
//   // ... your code ...
// };

// calculator.read();
// alert( calculator.sum() );
// alert( calculator.mul() );





let calculator = {
    read(){
        this.a = +prompt('a');
        this.b = +prompt('b');
    },
    sum(){
        return this.a + this.b;
    },
    mul(){
        return this.a* this.b;
    }
};


calculator.read();
alert( calculator.sum() );
alert( calculator.mul() );



// Chaining
// There’s a ladder object that allows to go up and down:

// let ladder = {
//   step: 0,
//   up() {
//     this.step++;
//   },
//   down() {
//     this.step--;
//   },
//   showStep: function() { // shows the current step
//     alert( this.step );
//   }
// };
// Now, if we need to make several calls in sequence, can do it like this:

// ladder.up();
// ladder.up();
// ladder.down();
// ladder.showStep(); // 1
// Modify the code of up, down and showStep to make the calls chainable, like this:

// ladder.up().up().down().showStep(); // 1
// Such approach is widely used across JavaScript libraries.





// let ladder = {
//     step:0,
//     up(){
//         this.step ++;
//         return this;
//     },
//     down(){
//         this.step --;
//         return this;
//     },
//     showStep(){
//         alert(this.step);
//         return this;
//     }
// }

// ladder.up().up().down().up().down().showStep();































