class GitHub {
    constructor() {
        this.client_id = '21efc642a14328c1805a';
        this.client_secret = '0d234984edf84620e706928a0ff39243e0768621';
    }

    async getUser (user){
        const profileResponse = await fetch(`https://api.github.com/users/${user}?client_id=${this.client_id}&client_secret=${this.client_secret}`);

        const profile = await profileResponse.json();

        return {
            profile
        }
    }
}