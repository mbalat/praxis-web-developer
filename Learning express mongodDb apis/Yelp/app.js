const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const Campground = require('./models/campground')
const Comment = require('./models/comment')
const User = require('./models/user')
const seedDB = require('./seeds')
const { render } = require('ejs')
const port = 3000

app.use(bodyParser.urlencoded({extended: true}))
app.set('view engine', 'ejs')
mongoose.connect('mongodb://localhost/yelpCamp',{useNewUrlParser: true, useUnifiedTopology: true})
app.use(express.static(`${__dirname}/public`))
seedDB()


app.get('/', (req, res) => {
    res.render('landing')
})

//  INDEX - show all
app.get('/campgrounds', (req, res) => {
    Campground.find({}, (err, allCampgrounds) => {
        if(err) {
            console.log(err)
        } else {
            res.render('campgrounds/index', {campgrounds: allCampgrounds})
        }
    })
})

// CREATE - add new to DB
app.post('/campgrounds', (req, res) => {
    let name = req.body.name
    let image = req.body.image
    let description = req.body.description
    let newCampground = {name: name, image: image, description: description}
    Campground.create(newCampground, (err, newlyCreated) => {
        if(err) {
            console.log(err)
        } else {
            res.redirect('/campgrounds')
        }
    })
})

//  NEW - show form
app.get('/campgrounds/new',(req, res) => {
   res.render("campgrounds/new")
})

//  SHOW
app.get('/campgrounds/:id',(req, res) => {
    Campground.findById(req.params.id).populate('comments').exec((err, foundCampground) => {
        if(err) {
            console.log(err)
        } else {
            res.render("campgrounds/show", {campground: foundCampground})
        }
    })
})

// COMMENTS ROUTES
app.get('/campgrounds/:id/comments/new', (req, res) => {
    //find route by id
    Campground.findById(req.params.id, (err, campground) => {
        if(err) {
            console.log(err)
        } else {
            res.render('comments/new', {campground: campground})
        }
    })
})

app.post('/campgrounds/:id/comments', (req, res) => {
    //lookup campground using ID
    Campground.findById(req.params.id, (err, campground) => {
        if(err) {
            console.log(err)
        } else {
            Comment.create(req.body.comment, (err, comment) => {
                if(err) {
                    console.log(err)
                } else {
                    campground.comments.push(comment)
                    campground.save()
                    res.redirect(`/campgrounds/${campground._id}`)
                }
            })
        }
    })
    //create new comment
    //connect new comment to campground
    //redirect campground show page
})

app.listen(port, ()=>{
    console.log(`http://localhost:${port}`)
})