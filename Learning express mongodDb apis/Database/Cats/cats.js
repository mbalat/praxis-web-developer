const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/cat_app', {useNewUrlParser: true, useUnifiedTopology: true})

const catSchema = new mongoose.Schema({
    name: String,
    age: Number,
    temperament: String
})

const Cat = mongoose.model("Cat", catSchema)

// const kitty = new Cat ({
//     name: 'Mr. Norris',
//     age: 11,
//     temperament: 'Evil'
// })

// kitty.save((err, cat) => {
//     if(err) {
//         console.log('Something went wrong');
//     } else {
//         console.log(`We just save your cat: ${cat.name} to db`);
//     }
// })



// Cat.create({
//     name: "Snow White",
//     age: 15,
//     temperament: 'Bland'
// }, (err,cat) => {
//     if(err) {
//         console.log(err)
//     } else {
//         console.log(cat)
//     }
// })

Cat.find({}, (err, cat)=> {
    if(err) {
        console.log(err);
    } else {
        cat.forEach(cat => {
            console.log(cat.name);
        })
    }
})