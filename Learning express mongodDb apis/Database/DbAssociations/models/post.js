const mongoose = require('mongoose')

//POST
const postSchema = new mongoose.Schema({
    title: String,
    content: String
})
const Post = mongoose.model('Post', postSchema)

module.exports = Post