const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const methodOverride = require('method-override')
const port = 3000

//  App config
mongoose.connect('mongodb://localhost/blog_app', {useNewUrlParser: true, useUnifiedTopology: true})
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(bodyParser.urlencoded({extended: true}))
app.use(methodOverride('_method'))

//  Mongoose model config
const blogShema = new mongoose.Schema({
    title: String,
    image: String,
    body: String,
    created: {type: Date, default: Date.now}
})

const Blog = mongoose.model('Blog', blogShema)

//  TEST
// Blog.create({
//     title: 'Test blog',
//     image: 'https://images.unsplash.com/photo-1548199973-03cce0bbc87b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
//     body: 'Test body text'
// })

//  Routes
app.get("/", (req, res) => {
    res.redirect('/blogs')
})

//  Index Route
app.get('/blogs', (req, res) => {
    Blog.find({}, (err, blogs) => {
        if(err) {
            console.log(err)
        } else {
            res.render('index', {blogs: blogs})
        }
    })
})

//  New Route
app.get("/blogs/new", (req, res) => {
    res.render('new')
})

// Create Route
app.post('/blogs', (req, res) => {
    //create blog
    Blog.create(req.body.blog, (err, newBlog) => {
        if(err) {
            res.render('new')
        } else {
            res.redirect('/blogs')
        }
    })
})

//  Show Route
app.get('/blogs/:id', (req, res) => {
    Blog.findById(req.params.id, (err, foundBlog) => {
        if(err) {
            console.log(err)
        } else {
            res.render('show', {blog: foundBlog})
        }
    })
})

//  Edit Route
app.get('/blogs/:id/edit', (req, res) => {
    Blog.findById(req.params.id, (err, foundBlog) => {
        if(err) {
            console.log(err)
        } else {
            res.render('edit', {blog: foundBlog})
        }
    })
})

//  Update Route
app.put('/blogs/:id', (req, res) => {
    Blog.findByIdAndUpdate(req.params.id, req.body.blog, (err, updatedBlog) => {
        if(err) {
            console.log(err)
        } else {
            res.redirect(`/blogs/${req.params.id}`)
        }
    })
})

//  Delete Route
app.delete('/blogs/:id', (req, res) => {
    Blog.findByIdAndRemove(req.params.id, (err) => {
        if(err) {
            console.log(err)
        } else {
            res.redirect('/blogs')
        }
    })
})

app.listen(port, () => {
    console.log(`http://localhost:${port}`)
})