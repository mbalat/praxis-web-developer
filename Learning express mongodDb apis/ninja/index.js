const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost/ninjago', { useNewUrlParser: true,  useUnifiedTopology: true})
mongoose.Promise = global.Promise
app.use(bodyParser.json())

//  IMPORT API ROUTE
const routes = require('./routes/api')

//  ERROR HANDLING MIDDLEWARE
app.use((err, req, res, next) => {
    res.status(422).send({error: err.message})
})

//  USE IMPORTS
app.use('/api', routes)


app.listen(5000)