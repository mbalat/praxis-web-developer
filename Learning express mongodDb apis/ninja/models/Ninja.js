const mongoose = require('mongoose')
  

    const geoSchema = new mongoose.Schema({
        type:{
            type: String,
            default: "Point"
        },
        coordinates:{
            type: [Number],
            index: "2dsphere"
        }
    })


const ninjaSchema = new mongoose.Schema({
    name:{
        type: String,
        required: [true, 'Name field is required']
    },
    rank: {
        type: String
    },
    available: {
        type: Boolean,
        default: false
    },
    geometry: geoSchema
})

//  Convert to model for DB collection
const Ninja = mongoose.model('ninja', ninjaSchema)

module.exports = Ninja