const express = require('express');
const router = express.Router()

//  IMPORT Schema
const Ninja = require('../models/Ninja')

//  GET LIST OF NINAJS FROM DB
router.get('/ninjas', (req, res, next) => {
    res.send('GET')
})

//  ADDING A NEW NINJA TO DB
router.post('/ninjas', (req, res, next) => {
    Ninja.create(req.body).then( (ninja) => {
        res.send(ninja)
    }).catch(next)
})

//  UPDATE A NINJA IN DB
router.put('/ninjas/:id', (req, res, next) => {
    Ninja.findByIdAndUpdate({_id: req.params.id}, req.body).then( () => {
        Ninja.findOne({_id: req.params.id}).then(ninja => {
            res.send(ninja)
        })
    })
})

//  DELETE A NJINJA FROM DB
router.delete('/ninjas/:id', (req, res, next) => {
    Ninja.findByIdAndRemove({_id: req.params.id}).then(ninja => {
        res.send(ninja)
    })

})

module.exports = router