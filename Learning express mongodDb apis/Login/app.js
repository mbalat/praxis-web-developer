const express = require("express")
const mongoose = require("mongoose")
const passport = require("passport")
const bodyParser = require("body-parser")
const User = require("./models/user")
const LocalStrategy = require("passport-local")
const passportLocalMongoose = require("passport-local-mongoose")
 
mongoose.connect('mongodb://localhost/auth_demo', { useNewUrlParser: true, useUnifiedTopology: true })
 
 
 
const app = express()
app.set("view engine", "ejs")
app.use(bodyParser.urlencoded({extended: true}))
app.use(require("express-session")({
    secret: "Biggie Smalls",
    resave: false,
    saveUninitialized: false
}))
app.use(passport.initialize())
app.use(passport.session())
passport.serializeUser(User.serializeUser())
passport.deserializeUser(User.deserializeUser())

//===============
//  ROUTES
//===============


app.get('/', (req, res) => {
    res.render('home')
})

app.get('/secret', (req, res) => {
    res.render('secret')
})

//===============
//  Auth routes
//===============
app.get('/register', (req, res) => {
    res.render('register')
})

app.post('/register', (req, res) => {
    
    User.register(new User({username: req.body.username}), req.body.password, (err, user) => {
        if(err) {
            console.log(err)
        } 
        passport.authenticate('local')(req, res, ()=> {
            res.redirect('/secret')
        })
    })
})

app.listen(3000, () => {
    console.log('http://localhost:3000')
})