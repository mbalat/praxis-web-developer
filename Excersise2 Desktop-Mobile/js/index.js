// 1. selektirati .js-copyright-year element
// 2. pronaći trenutnu godinu
// 3. postaviti trenutnu godinu kao sadržaj elementa


let currentYear = document.querySelector('.js-copyright-year');
let date = new Date();
let year = date.getFullYear();
currentYear.innerHTML = year;

// 0. postaviti hamburger button u header i postaviti hamburger izbornik
// 1. selektirati hamburger button
// 2. slušati pojavi click eventa na hamburger button
// 3. na click postaviti klasu "hamburger-open" na html ili body element
// 4. ako na html ili body elementu već postoji klasa "hamburger-open", na click maknuti klasu
// 5. ovisno o prisutnosti klase "hamburger-open" prikazati ili sakriti hamburger izbornik



document.querySelectorAll('.js-toggle-hamburger')
    .forEach(function (element) {
        element.addEventListener('click', addHamburgerClass);
    });

function addHamburgerClass(event){
    if (document.body.classList.contains('hamburger-open')) {
        document.body.classList.remove('hamburger-open');
        document.body.classList.add('hamburger-close');
        

        // 1. spremiti top položaj bodyja u varijablu
        // 2. maknuti position fixed sa body elementa
        // 3. odscrollati stranicu za top položaj bodyja

        let bodyTop = -parseFloat(document.body.style.top);
        document.body.style.position = 'initial';
        window.scrollTo(0, bodyTop);

    } else if (document.body.classList.contains('hamburger-close')) {
        document.body.classList.remove('hamburger-close');
        document.body.classList.add('hamburger-open');

        // 1. spremiti y položaj scrollbara u varijablu
        // 2. staviti body u position fixed
        // 3. staviti body top svojstvo u y položaj scrollbara

        let position = window.scrollY;
        document.body.style.position = 'fixed';
        document.body.style.top = -position + 'px';
        

        

    } else {
        document.body.classList.add('hamburger-open');
        document.querySelector('.header__navigation').style.display = 'block';
        // 1. spremiti y položaj scrollbara u varijablu
        // 2. staviti body u position fixed
        // 3. staviti body top svojstvo u y položaj scrollbara

        let position = window.scrollY;
        document.body.style.position = 'fixed';
        document.body.style.top = -position + 'px';
        
        
    }
}

// 1. slušati scroll event na windowu
// 2. ukoliko je scrollbar pomaknuti dalje od 0-tog položaja po y, staviti class "scrolled" na body
// 3. kada body ima klasu "scrolled", staviti header u fixed position (preko css-a)


const header = document.querySelector('.header-main');

window.addEventListener('scroll', function() {  
  if (window.scrollY > 0) {
    header.classList.add('scrolled');
  } else {
    header.classList.remove('scrolled');
  }
});